<?php
session_start();
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
if(isset($_REQUEST['id']))	{	
	  $tabid	=	$_REQUEST['id'];
	  }else{
		  $tabid = "LoRa_Tab_4";
	  } 
	  
	  
?>
<!DOCTYPE html>
<html>
<head>
 <title>HDS-1303NT</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="design.css">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script type="text/javascript" src="build/jquery.dialogextend.js"></script>
	<style>
	#myBtn {
		position: fixed;
		top: 10px;
		right: 10px;
		z-index: 99;
		font-size: 18px;
		width: 35px;
		height: 35px;
		border: none;
		outline: none;
		background-color: red;
		color: white;
		border-radius: 50%;
		
	}

	#myBtn:hover {
		
	}
	html,
	body {
		height: 100%;
		margin: 0;
		overflow: hidden;
		font-family: Arial, Helvetica, sans-serif;
	}

	.full-height {
		height: 100%;
	}

	input[type=text],
	select {
		width: 100%;
		padding: 12px 20px;
		margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
	}

	input[type=submit] {
		width: 100%;
		background-color: #4CAF50;
		color: white;
		padding: 14px 20px;
		margin: 8px 0;
		border: none;
		border-radius: 4px;
		cursor: pointer;
	}

	input[type=submit]:hover {
		background-color: #45a049;
	}

	.xdiv {
		background-color: #E0E0E0;
		padding: 20px;
	}

	.the-button {
		border-radius: 50%;
		box-shadow: 0 0 .5em rgba(0, 0, 0, 1), inset 0 .25em .3em rgba(255, 255, 255, .3), inset 0 -.25em .3em rgba(0, 0, 0, .5);
		cursor: pointer;
		font: inherit;
		height: 100%;
		padding: 0;
		position: relative;
		width: 100%;
	}

	.the-button:before {
		background: linear-gradient(to bottom, rgba(255, 255, 255, 0.14) 0%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, 0) 100%);
		border-radius: 50%;
		content: " ";
		height: 80%;
		left: 10%;
		position: absolute;
		top: 10%;
		width: 80%;
	}

	.the-button:active {
		box-shadow: 0 0 .5em rgba(0, 0, 0, 1), inset 0 .1em .3em rgba(0, 0, 0, .5), inset 0 -.1em .3em rgba(0, 0, 0, .5);
	}

	.the-button:active:before {
		top: 11.5%;
	}

	.button-frame {
		border-radius: 50%;
		box-shadow: 0 0 .25em rgba(0, 0, 0, .9), inset 0 .25em .125em rgba(255, 255, 255, .3), inset 0 -.25em .125em rgba(0, 0, 0, .3);
		display: block;
		height: 100px;
		margin: auto 0;
		padding: 8px;
		width: 100px;
		z-index: 20;
	}

	</style>
	<style>
	.GaugeMeter{
		Position:				Relative;
		Text-Align:			Center;
		Overflow:				Hidden;
		Cursor:				Default;
	}

	.GaugeMeter SPAN,
	.GaugeMeter B{
		Margin:				0 23%;
		Width:					54%;
		Position:				Absolute;
		Text-Align:			Center;
		Display:				Inline-Block;
		Color:					RGBa(0,0,0,.8);
		Font-Weight:			100;
		Font-Family:			"Open Sans", Arial;
		Overflow:				Hidden;
		White-Space:			NoWrap;
		Text-Overflow:		Ellipsis;
	}
	.GaugeMeter[data-style="Semi"] B{
		Margin:				0 10%;
		Width:					80%;
	}

	.GaugeMeter S,
	.GaugeMeter U{
		Text-Decoration:	None;
		Font-Size:			.60em;
		Font-Weight:			200;
		Opacity:				.6;
	}

	.GaugeMeter B{
		Color:					Black;
		Font-Weight:			200;
		Opacity:				.8;
	}
	</style>
	<style>
	#Header{
		min-Height:360px;
		height:100%;
	}

	.HEADER{
		z-Index:				1;
		Top:					0;
		Margin:				0;
		Text-Align:			Center;
		Background:			#21B4F9;
		Background:			#2C94E0;
		Background: 			Linear-Gradient(45deg, #2C94E0, #21B4F9), URL(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAnCAQAAAAmqpm+AAAAUUlEQVR4AWMgDvyXJEoV7/AwjnckGzdq3Khxo8apDQvj/kuCIS9OqIYCefFD4tzG+F+NGFW0Mo7ysBvNFaPGjRo3apzkIDfuP89/XipASQgEAF4fpQGYXVnnAAAAAElFTkSuQmCC);
		Background-Blend-Mode: Lighten;
		Box-Shadow:			0 0 20px 0 rgba(0,0,0,.4);
		-Moz-Animation:		BluePrint 60s Infinite;
		-Webkit-Animation:	BluePrint 60s Infinite;
		Animation:				BluePrint 60s Infinite;
	}

	@-moz-keyframes BluePrint{
			0%					{Background-Position: 0 0;}
		100%					{Background-Position: 1100% 100%;}
	}

	@-webkit-keyframes BluePrint{
			0%					{Background-Position: 0 0;}
		100%					{Background-Position: 1100% 100%;}
	}

	@keyframes BluePrint{
			0%					{Background-Position: 0 0;}
		100%					{Background-Position: -100% 100%;}
	}


	.HEADER .Preview{
		Overflow:				Auto;
		Margin-Top:			60px;
		Display:				Inline-Block;
	}

	.HEADER .Preview .GaugeMeter{
		Margin:				0 10px;
	}
	</style>
	<script src="jquery.AshAlom.gaugeMeter-2.0.0.min.js?201510211629"></script>
	
</head>
<body>
<script>
		$(document).ready(function(){
			$(".valds").click(function(){
				window.open('http://52.34.43.234/Okabe_Layout/detailed_info.php?unit_name='+'<?php echo $tabid; ?>');
			
			}
		);
				
			$(".centerDiv, .sideBarDiv").click(function(){
				var machineName	=	$(this).attr('id');
				
				$('<div id="frame3" class="divData" ><iframe id="frame3" src="settings.php?tabid=<?php echo $tabid; ?>" style="min-width:320px;min-height:245px" width="100%" height="100%"></iframe></div>').dialog({"resizable": false,"title":"Settings", close : function(){
                
				 reload_page();
              }  })
							.dialogExtend({
							"closable" : true,
							"maximizable" : false, 
								"icons" : { 
								  "close" : "ui-icon-circle-close",
								"maximize" : "ui-icon-circle-plus",
								"restore" : "ui-icon-bullet"
								},
								
							})
						$('.ui-dialog').css({"width":"410px","top":"60px","right":"50px"});
			}
				);
		});
		</script>
	<div style="width:100%;height:100%;float:left;" class="full-height">
		<div class="HEADER" id="Header">
			<div style="width:20%;float:left;background-color:white;border-radius:25px;margin:10px;height:96%">
				<div style="padding:10px;padding-left:10px;height:100%">
					<div style="height:25%">
						<div class="button-frame" id="r" style="margin:auto;background:#990000;" onclick="redx(this);">
							<div class="the-button"></div>
						</div>
					</div>
					<div style="height:25%">
						<div class="button-frame" id="y" style="margin:auto;background:#999900" onclick="yellowx(this);">
							<div class="the-button"></div>
						</div>
					</div>
					<div style="height:25%">
						<div class="button-frame" id="g" style="margin:auto;background:#194d19" onclick="greenx(this);">
							<div class="the-button"></div>
						</div>
					</div>
					<div style="height:25%">
						<div class="button-frame" id="b" style="margin:auto;background:#003d66" onclick="bluex(this);">
							<div class="the-button"></div>
						</div>
					</div>
					<!--<button id="btn1" class="valds" title="iot" >&#x2601;</button>-->
			
				</div>
				
			</div>
			<div style="width:10%;height:2%;float:right;margin:10px;margin-top:2px" id="setbuttons">
			</div>
			
			<div class="Preview" style="" id="boxmet">
				<div class="GaugeMeter" style="float:left" id="PreviewGaugeMeter_3" data-percent="23" data-append="&#8451;" data-size="200" data-theme="White" data-back="RGBa(0,0,0,.1)" data-animate_gauge_colors="0" data-animate_text_colors="1" data-width="15" data-label="Temperature" data-style="Arch" data-label_color="#FFF"></div>
				<div class="GaugeMeter" style="float:left" id="PreviewGaugeMeter_4" data-percent="55" data-append="dbm" data-size="180" data-theme="White" data-back="RGBa(0,0,0,.1)" data-animate_gauge_colors="0" data-animate_text_colors="1" data-width="15" data-label="Sound" data-label_color="#FFF" data-stripe="2"></div>
				<div class="GaugeMeter" style="float:left" id="PreviewGaugeMeter_2" data-percent="88" data-append="%" data-size="200" data-theme="White" data-back="RGBa(0,0,0,.1)" data-animate_gauge_colors="0" data-animate_text_colors="1" data-width="15" data-label="Humidity" data-style="Arch" data-label_color="#FFF"></div>
			
			<img src="iot.png" class="valds" alt="iot" height="50" width="50"/>
			
			
			</div>
			<div style="width:75%;height:32%;float:right;margin:10px;margin-top:80px" id="rfid">
			</div>
			
		</div>
		
	</div>
	
<script>
var red=0;
var yellow=0;
var green=0;
var blue=0;
var bnk="0 0 .25em rgba(0,0,0,.9), inset 0 .25em .125em rgba(255,255,255,.3), inset 0 -.25em .125em rgba(0,0,0,.3)";
var bnk1=", 0 0 .25em rgba(0,0,0,.9), inset 0 .25em .125em rgba(255,255,255,.3), inset 0 -.25em .125em rgba(0,0,0,.3)";
function redx(x) {
    if (!red) {
		
sessionStorage.setItem("<?php echo $tabid; ?>pressed", "red");
		bac();
        x.style.boxShadow="0 0 8em rgba(255,0,0,1)"+bnk1;
		x.style.backgroundColor="#ff0000";
        red=1;
        yellow=0;
        green=0;
        blue=0;
	sendval();
    }
    else {
		sessionStorage.setItem("<?php echo $tabid; ?>pressed", "red1");
        bac();
        red=0;
		sendval();
    }
}

function yellowx(x) {
    if (!yellow) {
		sessionStorage.setItem("<?php echo $tabid; ?>pressed", "yellow");
		bac();
        x.style.boxShadow="0 0 8em rgba(255,255,0,1)"+bnk1;
		x.style.backgroundColor="yellow";
        red=0;
        yellow=1;
        green=0;
        blue=0;
	sendval();
    }
    else {
		sessionStorage.setItem("<?php echo $tabid; ?>pressed", "yellow1");
        bac();
        yellow=0;
		sendval();
    }
}

function greenx(x) {
    if (!green) {sessionStorage.setItem("<?php echo $tabid; ?>pressed", "green");
		bac();
        x.style.boxShadow="0 0 8em rgba(0,255,0,1)"+bnk1;
		x.style.backgroundColor="#00ff00";
        red=0;
        yellow=0;
        green=1;
        blue=0;
	sendval();
    }
    else {sessionStorage.setItem("<?php echo $tabid; ?>pressed", "green1");
        bac();
        green=0;
		sendval();
    }
}

function bluex(x) {
    if (!blue) {sessionStorage.setItem("<?php echo $tabid; ?>pressed", "blue");
		bac();
        x.style.boxShadow="0 0 8em rgba(0,0,255,1)"+bnk1;
		x.style.backgroundColor="#0099ff";
        red=0;
        yellow=0;
        green=0;
        blue=1;
			sendval();
    }
    else {sessionStorage.setItem("<?php echo $tabid; ?>pressed", "blue1");
        bac();
        blue=0;
		sendval();
    }
	//..........................ajax

	 
}
function bac()
{
	document.getElementById("r").style.backgroundColor="#990000";
	document.getElementById("r").style.boxShadow=bnk;
	document.getElementById("y").style.backgroundColor="#999900";
	document.getElementById("y").style.boxShadow=bnk;
	document.getElementById("g").style.backgroundColor="#194d19";
	document.getElementById("g").style.boxShadow=bnk;
	document.getElementById("b").style.backgroundColor="#003d66";
	document.getElementById("b").style.boxShadow=bnk;
}

function sendval(){


var xhttp = new XMLHttpRequest();
	var up_user1=0;
	var up_user2=0;
	var up_user3=0;
	var up_user4=0;
	
	
	if(dis_stat_1==1){
		up_user1 = document.getElementById('ix1').value;
		if(up_user1==""){
			up_user1 =0;
		}
	}
	if(dis_stat_2==1){
		up_user2 = document.getElementById('ix2').value;
		if(up_user2==""){
			up_user2 =0;
		}
	}
	if(dis_stat_3==1){
		up_user3 = document.getElementById('ix3').value;
		if(up_user3==""){
			up_user3 =0;
		}
	}
	
	if(dis_stat_4==1){
		up_user4 = document.getElementById('ix4').value;
		if(up_user4==""){
			up_user4 =0;
		}
	}
	
	var user_val_tot = up_user1+","+up_user2+","+up_user3+","+up_user4;
	var user_val_tot = user_val_tot.replace("0,", "");
	var user_val_tot = user_val_tot.replace("0,", "");
	var user_val_tot = user_val_tot.replace("0,", "");
	var user_val_tot = user_val_tot.replace(",0", "");
	
	if(user_val_tot == 0){
		user_val_tot ="";
	}
	
    xhttp.open("get", "http://52.34.43.234/Okabe_Layout/operator_new_changes_login_db.php?layout=Okabe&unit="+"<?php echo $tabid; ?>"+"&temp=24&hum=48&sound=200&cus_id="+user_val_tot+"&a="+red+"&s="+blue+"&r="+yellow+"&n="+green, true);
 // alert( "http://52.34.43.234/Okabe_Layout/operator_new_changes_login_db.php?layout=Okabe&unit="+"<?php echo $tabid; ?>"+"&temp=24&hum=48&sound=200&cus_id="+user_val_tot+"&a="+red+"&s="+blue+"&r="+yellow+"&n="+green);
 xhttp.onreadystatechange = function () {
  if(xhttp.status == 200) {
	  console.log(xhttp.status);
  }else{
	  console.log(xhttp.status);
	  console.log("Fails");
	  sendval();
  }
};
  
  xhttp.send(null);
}
</script>
<?php 
 $username = "root";
      $password = "";
      $host = "127.0.0.1";
 $connector = mysqli_connect($host,$username,$password)
          or die("Unable to connect");
        echo "Connections are made successfully::";
      $selected = mysqli_select_db($connector,"setupsheetdb")
        or die("Unable to connect");
		$query = "SELECT * FROM testiotuser where logstatus=1 order by Id Asc";
	$val=1;
	$user1=0;
	$stat1=0;
	$dislog1=0;
	$user2=0;
	$stat2=0;
	$dislog2=0;
	$user3=0;
	$stat3=0;
	$dislog3=0;
	$user4=0;
	$stat4=0;
	$dislog4=0;
	
	$userid1=0;
	$userid2=0;
	$userid3=0;
	$userid4=0;
  if ($result = $connector->query($query)) {
    while ($row = $result->fetch_assoc()) {
		
        $field1name = $row["id"];
        $fname = $row["name"];
        $field3name = $row["imgname"]; 
		$status_log = $row["status"]; 
		$display_log = $row["logstatus"]; 
		echo $fname;
		if($val==2 && ($display_log==1 && $fname!="" )){
			$dislog2=1;
			if($status_log ==1){
				$stat2 =1;
			}else{
				$stat2 =0;
			}
			$user2=$fname;
			$userid2=$field1name;
		}else if($val==2 && ($display_log==0 ||$fname=="" )){$user2=0;$stat2 =0;$dislog2=0;}

		else if($val==3 && ($display_log==1 && $row["name"]!="" )){
			$dislog3=1;
			if($status_log ==1){
				$stat3 =1;
			}else{
				$stat3 =0;
			}
			$user3=$fname;
			$userid3=$field1name;
		}else if($val==3 && ($display_log==0 ||$fname=="" )){$user3=0;$stat3 =0;$dislog3=0;}

		else if($val==4 && ($display_log==1 && $fname!="" )){
			$dislog4=1;
			if($status_log ==1){
				$stat4 =1;
			}else{
				$stat4 =0;
			}
			$user4=$fname;
			$userid4=$field1name;
		}else if($val==4 && ($display_log==0 ||$fname=="" )){$user4=0;
		$stat4 =0;$dislog4=0;
		}
		
		else if($val==1 && ($display_log==1 && $fname!="" )){
			$dislog1=1;
			if($status_log ==1){
				$stat1 =1;
			}else{
				$stat1 =0;
			}
			$user1=$fname;
			
			$userid1=$field1name;
		}else if($val==1 && ($display_log==0 ||$fname=="" )){$user1=0;
		$stat1 =0;$dislog1=0;}
		
    $val++;
    }
}

?>

<script>
var reload=0;
var amjtab;
var rfidback1="0";
var rfidback2="0";
var rfidback3="0";
var rfidback4="0";
var rfidbacklast="";
var stateofid=1;

var user_1= "<?php echo $user1; ?>";
var user_2= "<?php echo $user2; ?>";
var user_3= "<?php echo $user3; ?>";
var user_4= "<?php echo $user4; ?>";

var userid_1= "<?php echo $userid1; ?>";
var userid_2= "<?php echo $userid2; ?>";
var userid_3= "<?php echo $userid3; ?>";
var userid_4= "<?php echo $userid4; ?>";

var stat_1= "<?php echo $stat1; ?>";
var stat_2= "<?php echo $stat2; ?>";
var stat_3= "<?php echo $stat3; ?>";
var stat_4= "<?php echo $stat4; ?>";


var dis_stat_1= "<?php echo $dislog1; ?>";
var dis_stat_2= "<?php echo $dislog2; ?>";
var dis_stat_3= "<?php echo $dislog3; ?>";
var dis_stat_4= "<?php echo $dislog4; ?>";

if(user_1!='0' && stat_1 ==1){
document.getElementById("rfid").innerHTML+='<div style="width:1%;height:100%;float:left" id="by1"> </div><div id="bx1"  style="width:24%;height:100%;background-color:#ECEFF1;float:left;border-radius:25px;background-color:white;">'+user_1+'<br><input id="ix1" type="hidden" name="result" value="'+user_1+'"><img src="user.jpg" alt="Snow" style="width:60%;margin-top:5px"><button id="lg1" style="width:80%;border-radius:10px;padding:10px;background-color:#FF5C7C;color:white" value="Logout" onclick="updatex(\'bx1\');">Logout</button></div>';

}else if(user_1!='0' && stat_1 ==0){
	document.getElementById("rfid").innerHTML+='<div style="width:1%;height:100%;float:left" id="by1"> </div><div id="bx1"  style="width:24%;height:100%;background-color:#ECEFF1;float:left;border-radius:25px;background-color:#A9A9A9;">'+user_1+'<br><input id="ix1" type="hidden" name="result" value="0"><img src="user.jpg" alt="Snow" style="width:60%;margin-top:5px"><button id="lg1" style="width:80%;border-radius:10px;padding:10px;background-color:#DD732C;color:white" value="Login" onclick="updatex(\'bx1\');">Login</button></div>';
user_1=0;
	}


if(user_2!='0' && stat_2 ==1){
document.getElementById("rfid").innerHTML+='<div style="width:1%;height:100%;float:left" id="by2"> </div><div id="bx2"  style="width:24%;height:100%;background-color:#ECEFF1;float:left;border-radius:25px;background-color:white;">'+user_2+'<br><input id="ix2" type="hidden" name="result" value="'+user_2+'"><img src="user.jpg" alt="Snow" style="width:60%;margin-top:5px"><button id="lg2" style="width:80%;border-radius:10px;padding:10px;background-color:#FF5C7C;color:white" value="Logout" onclick="updatex(\'bx2\');">Logout</button></div>';
}else if(user_2!='0' && stat_2==0){
	document.getElementById("rfid").innerHTML+='<div style="width:1%;height:100%;float:left" id="by2"> </div><div id="bx2"  style="width:24%;height:100%;background-color:#ECEFF1;float:left;border-radius:25px;background-color:#A9A9A9;">'+user_2+'<br><input id="ix2" type="hidden" name="result" value="0"><img src="user.jpg" alt="Snow" style="width:60%;margin-top:5px"><button id="lg2" style="width:80%;border-radius:10px;padding:10px;background-color:#DD732C;color:white"  value="Login" onclick="updatex(\'bx2\');">Login</button></div>';
user_2=0;
}

if(user_3!='0' && stat_3 ==1){
document.getElementById("rfid").innerHTML+='<div style="width:1%;height:100%;float:left" id="by3"> </div><div id="bx3" style="width:24%;height:100%;background-color:#ECEFF1;float:left;border-radius:25px;background-color:white;">'+user_3+'<br><input id="ix3" type="hidden" name="result" value="'+user_3+'"><img src="user.jpg" alt="Snow" style="width:60%;margin-top:5px"><button id="lg3" style="width:80%;border-radius:10px;padding:10px;background-color:#FF5C7C;color:white" value="Logout" onclick="updatex(\'bx3\');">Logout</button></div>';
}else if(user_3!='0' && stat_3==0){
	document.getElementById("rfid").innerHTML+='<div style="width:1%;height:100%;float:left;" id="by3"> </div><div id="bx3" style="width:24%;height:100%;background-color:#ECEFF1;float:left;border-radius:25px;background-color:#A9A9A9;">'+user_3+'<br><input id="ix3" type="hidden" name="result" value="0"><img src="user.jpg" alt="Snow" style="width:60%;margin-top:5px"><button id="lg3"style="width:80%;border-radius:10px;padding:10px;background-color:#DD732C;color:white" value="Login" onclick="updatex(\'bx3\');">Login</button></div>';
user_3=0;
}


if(user_4!='0' && stat_4 ==1){
	document.getElementById("rfid").innerHTML+='<div style="width:1%;height:100%;float:left" id="by4"> </div><div id="bx4"  style="width:24%;height:100%;background-color:#ECEFF1;float:left;border-radius:25px;background-color:white;">'+user_4+'<br><input id="ix4" type="hidden" name="result" value="'+user_4+'"><img src="user.jpg" alt="Snow" style="width:60%;margin-top:5px"><button id="lg4" style="width:80%;border-radius:10px;padding:10px;background-color:#FF5C7C;color:white" value="Logout" onclick="updatex(\'bx4\');">Logout</button></div>';
}else if(user_4!='0' && stat_4==0){
	document.getElementById("rfid").innerHTML+='<div style="width:1%;height:100%;float:left" id="by4"> </div><div id="bx4"  style="width:24%;height:100%;background-color:#ECEFF1;float:left;border-radius:25px;background-color:#A9A9A9;">'+user_4+'<br><input id="ix4" type="hidden" name="result" value="0"><img src="user.jpg" alt="Snow" style="width:60%;margin-top:5px"><button id="lg4" style="width:80%;border-radius:10px;padding:10px;background-color:#DD732C;color:white" value="Login" onclick="updatex(\'bx4\');">Login</button></div>';
user_4=0;
}
//outputamj('7685749284');
function outputamj(y)
{
	var newstr;
	var statexyz=0;
	if((y[1].includes("(")==true)||(y[1].includes(")")==true))
	{
		var firstIndex = y[1].indexOf(")")+1;
		newstr = y[1].substring(firstIndex);
		
		if(newstr.length == 12)
		{
			if(newstr.includes("<")||newstr.includes(">")||newstr.includes("$")||newstr.includes("%")||newstr.includes("&")||newstr.includes(" "))
			{
				statexyz=0;
			}
			else
			{
				y[1]=newstr;
				statexyz=1;
				console.log("x");
			}
			
		}
		else
		{
			console.log("y");
			statexyz=0;
		}		
	}
	else
	{
		console.log("z");
		statexyz=0;
	}
	
	if(statexyz==0)
	{
		if(rfidbacklast==y[1])
		{
			
		}
		else if(rfidback1==y[1]||rfidback2==y[1]||rfidback3==y[1]||rfidback4==y[1])
		{
			rfidbacklast=y[1];
		}
		else if(rfidback1=='0')
		{
			rfidback1=y[1];
			stateofid=2;
			document.getElementById("rfid").innerHTML+='<div style="width:1%;height:100%;float:left" id="by1"> </div><div id="bx1" style="width:24%;height:100%;background-color:#ECEFF1;float:left;border-radius:25px;background-color:white;">'+y[1]+'<br><img src="user.jpg" alt="Snow" style="width:60%;margin-top:5px"><button style="width:80%;border-radius:10px;padding:10px;background-color:#FF5C7C;color:white" onclick="removex(\'bx1\');">Logout</button></div>';

		}
		else if(rfidback2=='0')
		{
			rfidback2=y[1];
			stateofid=3;
			document.getElementById("rfid").innerHTML+='<div style="width:1%;height:100%;float:left" id="by2"> </div><div id="bx2" style="width:24%;height:100%;background-color:#ECEFF1;float:left;border-radius:25px;background-color:white;">'+y[1]+'<br><img src="user.jpg" alt="Snow" style="width:60%;margin-top:5px"><button style="width:80%;border-radius:10px;padding:10px;background-color:#FF5C7C;color:white" onclick="removex(\'bx2\');">Logout</button></div>';

		}
		else if(rfidback3=='0')
		{
			rfidback3=y[1];
			stateofid=4;
			document.getElementById("rfid").innerHTML+='<div style="width:1%;height:100%;float:left" id="by3"> </div><div id="bx3" style="width:24%;height:100%;background-color:#ECEFF1;float:left;border-radius:25px;background-color:white;">'+y[1]+'<br><img src="user.jpg" alt="Snow" style="width:60%;margin-top:5px"><button style="width:80%;border-radius:10px;padding:10px;background-color:#FF5C7C;color:white" onclick="removex(\'bx3\');">Logout</button></div>';

		}
		else if(rfidback4=='0')
		{
			rfidback4=y[1];
			stateofid=1;
			document.getElementById("rfid").innerHTML+='<div style="width:1%;height:100%;float:left" id="by4"> </div><div id="bx4" style="width:24%;height:100%;background-color:#ECEFF1;float:left;border-radius:25px;background-color:white;">'+y[1]+'<br><img src="user.jpg" alt="Snow" style="width:60%;margin-top:5px"><button style="width:80%;border-radius:10px;padding:10px;background-color:#FF5C7C;color:white" onclick="removex(\'bx4\');">Logout</button></div>';

		}
		rfidbacklast=y[1];
		document.getElementById('boxmet').innerHTML='<div class="GaugeMeter" style="float:left" id="PreviewGaugeMeter_3" data-percent="23" data-append="&#8451;" data-size="200" data-theme="White" data-back="RGBa(0,0,0,.1)" data-animate_gauge_colors="0" data-animate_text_colors="1" data-width="15" data-label="Temperature" data-style="Arch" data-label_color="#FFF"></div><div class="GaugeMeter" style="float:left" id="PreviewGaugeMeter_4" data-percent="55" data-append="dbm" data-size="180" data-theme="White" data-back="RGBa(0,0,0,.1)" data-animate_gauge_colors="0" data-animate_text_colors="1" data-width="15" data-label="Sound" data-label_color="#FFF" data-stripe="2"></div><div class="GaugeMeter" style="float:left" id="PreviewGaugeMeter_2" data-percent="88" data-append="%" data-size="200" data-theme="White" data-back="RGBa(0,0,0,.1)" data-animate_gauge_colors="0" data-animate_text_colors="1" data-width="15" data-label="Humidity" data-style="Arch" data-label_color="#FFF"></div>';
		document.getElementById("PreviewGaugeMeter_3").dataset.percent=y[2];//...temp
		document.getElementById("PreviewGaugeMeter_2").dataset.percent=y[3];//...hum
		document.getElementById("PreviewGaugeMeter_4").dataset.percent=y[4];//...sou
		var script= document.getElementById('scrx').innerHTML;
		eval(script);
	}
}
function reload_page() {

	location.reload();
	
	
}
set_vaues_fn();
function set_vaues_fn(){
	var value = sessionStorage.getItem("<?php echo $tabid; ?>pressed");
	
	if(value =='red'){
		//alert(value);
		bac();
        document.getElementById('r').style.boxShadow="0 0 8em rgba(255,0,0,1)"+bnk1;
		document.getElementById('r').style.backgroundColor="#ff0000";
        red=1;
        yellow=0;
        green=0;
        blue=0;
	}else if(value=='red1'){
		 bac();
        red=0;
    }else if(value=='yellow') {
		bac();
        document.getElementById('y').style.boxShadow="0 0 8em rgba(255,255,0,1)"+bnk1;
		document.getElementById('y').style.backgroundColor="yellow";
        red=0;
        yellow=1;
        green=0;
        blue=0;
    }
    else if(value=='yellow1') {
        bac();
        yellow=0;
    }else if(value=='green') {
		bac();
        document.getElementById('g').style.boxShadow="0 0 8em rgba(0,255,0,1)"+bnk1;
		document.getElementById('g').style.backgroundColor="#00ff00";
        red=0;
        yellow=0;
        green=1;
        blue=0;
    }else if(value=='green1') {
        bac();
        green=0;
    }else if(value=='blue') {
		bac();
        document.getElementById('b').style.boxShadow="0 0 8em rgba(0,0,255,1)"+bnk1;
		document.getElementById('b').style.backgroundColor="#0099ff";
        red=0;
        yellow=0;
        green=0;
        blue=1;
    }else if(value=='blue1'){
        bac();
        blue=0;
    }
}
function relodeini() {
    reload=1;
}

</script>
<script> 
  document.getElementById("setbuttons").innerHTML += '<button id="myBtn" class="centerDiv" title="settings" >&#9881;</button>';
reload = 0;

function reloadamj() {
    inputmamj();
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if (reload == 1) {
                location.reload();
            }
            var x = this.responseText;
            var y = x.split('&');
            if (y[0] == '1') {
                relodeini();
            }
            outputamj(y);
        }
    };
    inputmamj();
    xhttp.open("post", "http://192.168.4.1/com", true);
  xhttp.send(amjtab);
}
</script>
<script id="scrx">
$(document).ready(function(){
	//GaugeMeter
	$(".GaugeMeter").gaugeMeter();



	//Format Code
	$("pre.Code").html(function (index, html) {
		return html.replace(/^(.*)$/mg, "<span class='Line'>$1</span>")
	});



	//Sticky Table Header
	var tables = $("table.StickyHeader");
	tables.each(function(i){
		var table = tables[i];
		var theadClone = $(table).find("thead").clone(true);
		var stickyHeader =  $("<div></div>").addClass("StickyHeader Hide");
		stickyHeader.append($("<table></table")).find("table").append(theadClone);
		$(table).after(stickyHeader);

		var tableHeight = $(table).height();
		var tableWidth = $(table).width() + Number($(table).css("padding-left").replace(/px/ig,"")) + Number($(table).css("padding-right").replace(/px/ig,"")) + Number($(table).css("border-left-width").replace(/px/ig,"")) + Number($(table).css("border-right-width").replace(/px/ig,""));

		var headerCells = $(table).find("thead th");
		var headerCellHeight = $(headerCells[0]).height();

		var no_fixed_support = false;
		if (stickyHeader.css("position") == "Absolute"){
			no_fixed_support = true;
		}

		var stickyHeaderCells = stickyHeader.find("th");
		//stickyHeader.css("width", tableWidth);
		stickyHeader.css("width", "100%");

		/*
		for (i=0; i<headerCells.length; i++) {
			var headerCell = $(headerCells[i]);
			var cellWidth = headerCell.width();
			$(stickyHeaderCells[i]).css("width", cellWidth);
		}
		*/
		var cellWidth = $(headerCells[0]).width() + 1;
		$(stickyHeaderCells[0]).attr("style", "width:" + cellWidth + "px !important");

		var cutoffTop = $(table).offset().top;
		var cutoffBottom = tableHeight + cutoffTop - headerCellHeight;

		$(window).scroll(function(){ 
		var currentPosition = $(window).scrollTop();
			if(currentPosition > cutoffTop && currentPosition < cutoffBottom){
				stickyHeader.removeClass("Hide");
				if (no_fixed_support){
					stickyHeader.css("top", currentPosition + "px");
				}
			}else{
				stickyHeader.addClass("Hide");
			}
		});
	});



});


</script>
<script>

function reloadpage(){
	location.reload(true);
}

function updatex(x)
{
	if(x=="bx1")
	{
		var button_value=document.getElementById('lg1').value;

		if(button_value == 'Login'){
			
		document.getElementById('ix1').value="<?php echo $user1;?>";;

		rfidback1="0";
		document.getElementById('bx1').style.backgroundColor="#FFFFFF";
		document.getElementById('lg1').style.backgroundColor="#FF5C7C";
		document.getElementById('lg1').value  = 'Logout';
		document.getElementById('lg1').innerHTML  = 'Logout';
		 var http = new XMLHttpRequest(); 
    http.open("GET", "/updatelogin.php?sid=<?php echo $userid1;?>", true);
	 http.send(null);
		}
		else{
		document.getElementById('ix1').value='';
			rfidback1="0";
		 document.getElementById('bx1').style.backgroundColor="#A9A9A9";
        document.getElementById('lg1').style.backgroundColor="#DD732C";
		document.getElementById('lg1').innerHTML  = 'Login';
		document.getElementById('lg1').value  = 'Login';
		var http = new XMLHttpRequest(); 
    http.open("GET", "/updatelogout.php?sid=<?php echo $userid1;?>", true);
    http.send(null);
		}
		
	}
	else if(x=="bx2")
	{var button_value=document.getElementById('lg2').value;
		if(button_value == 'Login'){
		document.getElementById('ix2').value="<?php echo $user2;?>";
		rfidback2="0";
		
	document.getElementById('bx2').style.backgroundColor="#FFFFFF";
		document.getElementById('lg2').style.backgroundColor="#FF5C7C";
		document.getElementById('lg2').value  = 'Logout';
		document.getElementById('lg2').innerHTML  = 'Logout';
		var http = new XMLHttpRequest(); 
    http.open("GET", "/updatelogin.php?sid=<?php echo $userid2;?>", true);
    http.send(null);
		
		}else{
		document.getElementById('ix2').value='';
			rfidback2="0";
		 // get the URL
   
		//document.getElementById("by1").remove();
		
			document.getElementById('bx2').style.backgroundColor="#A9A9A9";
        document.getElementById('lg2').style.backgroundColor="#DD732C";
		document.getElementById('lg2').innerHTML  = 'Login';
		document.getElementById('lg2').value  = 'Login';
		var http = new XMLHttpRequest(); 
    http.open("GET", "/updatelogout.php?sid=<?php echo $userid2;?>", true);
    http.send(null);
		
		}
		//document.getElementById("by2").remove();
		
	}
	else if(x=="bx3")
	{var button_value=document.getElementById('lg3').value;
		if(button_value == 'Login'){
		document.getElementById('ix3').value="<?php echo $user3;?>";
		rfidback3="0";
		
	document.getElementById('bx3').style.backgroundColor="#FFFFFF";
		document.getElementById('lg3').style.backgroundColor="#FF5C7C";
		document.getElementById('lg3').value  = 'Logout';
		document.getElementById('lg3').innerHTML  = 'Logout';
		var http = new XMLHttpRequest(); 
    http.open("GET", "/updatelogin.php?sid=<?php echo $userid3;?>", true);
    http.send(null);
		}else{
		document.getElementById('ix3').value='';
			rfidback3="0";
		 // get the URL
   
		//document.getElementById("by1").remove();
		
			document.getElementById('bx3').style.backgroundColor="#A9A9A9";
        document.getElementById('lg3').style.backgroundColor="#DD732C";
		document.getElementById('lg3').innerHTML  = 'Login';
		document.getElementById('lg3').value  = 'Login';
		 var http = new XMLHttpRequest(); 
    http.open("GET", "/updatelogout.php?sid=<?php echo $userid3;?>", true);
    http.send(null);
		}
		//document.getElementById("by3").remove();
		
	}
	else if(x=="bx4")
	{var button_value=document.getElementById('lg4').value;
		if(button_value == 'Login'){
		document.getElementById('ix4').value="<?php echo $user4;?>";
		rfidback4="0";
		
	document.getElementById('bx4').style.backgroundColor="#FFFFFF";
		document.getElementById('lg4').style.backgroundColor="#FF5C7C";
		document.getElementById('lg4').value  = 'Logout';
		document.getElementById('lg4').innerHTML  = 'Logout';
		var http = new XMLHttpRequest(); 
    http.open("GET", "/updatelogin.php?sid=<?php echo $userid4;?>", true);
    http.send(null);
		}else{
		document.getElementById('ix4').value='';
			rfidback4="0";
		 // get the URL
   
		//document.getElementById("by1").remove();
		
			document.getElementById('bx4').style.backgroundColor="#A9A9A9";
        document.getElementById('lg4').style.backgroundColor="#DD732C";
		document.getElementById('lg4').innerHTML  = 'Login';
		document.getElementById('lg4').value  = 'Login';
		 var http = new XMLHttpRequest(); 
    http.open("GET", "/updatelogout.php?sid=<?php echo $userid4;?>", true);
    http.send(null);
		
		}
		//document.getElementById("by4").remove();
	}
	
	//document.getElementById(x).remove();
	
	setTimeout(function () {
		sendval();		
	//	reload_page();
		}, 70);
}
</script>

</body>
</html>